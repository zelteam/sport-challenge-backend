package com.grined.controller

import com.grined.entity.Training
import com.grined.entity.TrainingMetaInfo
import com.grined.entity.User
import com.grined.repository.TrainingRepository
import com.grined.repository.UserRepository
import com.mongodb.client.gridfs.model.GridFSFile
import org.apache.tomcat.util.http.fileupload.IOUtils
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.gridfs.GridFsTemplate
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.time.Instant
import javax.servlet.http.HttpServletResponse


@RestController
@RequestMapping("/api/v1/training")
class TrainingController(val userRepository: UserRepository,
                         val trainingRepository: TrainingRepository,
                         val gridFsTemplate: GridFsTemplate) : BaseController(userRepository) {
    @PostMapping
    fun addTraining(@RequestBody newTraining: TrainingMetaInfo): TrainingDto {
        return buildTrainingDto(
                trainingRepository.save(Training(
                        userId = getUser().id!!,
                        metaInfo = newTraining,
                        time = Instant.now())))
    }

    @PostMapping("/{id}/photo")
    fun addTrainingPhoto(@PathVariable id: String, @RequestParam file: MultipartFile) {
        val training = findTraining(id)
        val userId = getUser().id
        if (training.userId != userId) throw AccessDenied("User ")
        val stored = gridFsTemplate.store(file.inputStream, file.originalFilename!!)
        trainingRepository.save(training.copy(photoId = stored.toString()))
    }

    @GetMapping("/{id}/photo")
    fun getTrainingPhoto(@PathVariable id: String, response: HttpServletResponse) {
        val training = findTraining(id)
        val fileId : GridFSFile? = gridFsTemplate.findOne(Query(Criteria.where("_id").`is`(training.photoId)))
        if (fileId != null) {
            IOUtils.copy(gridFsTemplate.getResource(fileId.filename).inputStream, response.outputStream)
        }
    }

    @GetMapping("/all")
    fun findAllTrainings(): List<TrainingDto> {
        return trainingRepository.findAll(PageRequest.of(0, 100, Sort.Direction.DESC, "time"))
                .map { buildTrainingDto(it) }
                .toList()
    }

    private fun findTraining(id: String) =
            trainingRepository.findById(id).orElseThrow { ResourceNotFoundException("No training with id = {$id}") }

    fun buildTrainingDto(t: Training): TrainingDto =
            TrainingDto(id = t.id!!, userId = t.userId, likes = t.likes, disputes = t.disputes,
                    time = t.time, metaInfo = t.metaInfo!!,
                    username = userRepository.findById(t.userId).map(User::username).orElse(""))

    data class TrainingDto(val userId: String,
                           val id: String,
                           val username: String,
                           val likes: Int,
                           val disputes: Int,
                           val time: Instant,
                           val metaInfo: TrainingMetaInfo)
}


