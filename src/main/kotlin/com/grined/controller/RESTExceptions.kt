package com.grined.controller

import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(NOT_FOUND)
class ResourceNotFoundException(msg: String) : IllegalArgumentException(msg)

@ResponseStatus(FORBIDDEN)
class AccessDenied(msg: String) : IllegalArgumentException(msg)