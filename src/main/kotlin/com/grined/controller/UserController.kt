package com.grined.controller

import com.grined.entity.User
import com.grined.repository.UserRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/user")
class UserController(val userRepository: UserRepository,
                     val passwordEncoder: PasswordEncoder) : BaseController(userRepository) {
    @PostMapping("/create")
    fun createUser(@RequestBody user: UserInputDto) {
        userRepository.save(User(
                username = user.username,
                password = passwordEncoder.encode(user.password)))
    }

    @GetMapping("/me")
    fun me() = UserOutputDto(getUser().username)

    @GetMapping("/all")
    fun getUsers() : List<UserOutputDto> = userRepository.findAll()
            .map { u -> UserOutputDto(u.username) }

    data class UserInputDto(val username: String, val password: String)
    data class UserOutputDto(val username: String)
}