package com.grined.controller

import com.grined.entity.User
import com.grined.repository.UserRepository
import org.springframework.security.core.context.SecurityContextHolder


abstract class BaseController(private val userRepository : UserRepository) {
    fun getUser() : User {
        val auth = SecurityContextHolder.getContext().authentication
        return userRepository.findByUsername(auth.name)!!
    }
}
