package com.grined.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class OpenStub {
    @GetMapping("/open")
    fun test(): TestData {
        return TestData(
                name = UUID.randomUUID().toString(),
                auth = Random().nextBoolean(),
                likes = Random().nextInt(100))
    }

    data class TestData(val name: String, val auth: Boolean, val likes: Int)

}