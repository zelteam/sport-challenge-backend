package com.grined

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SportChallengeApplication

fun main(args: Array<String>) {
    runApplication<SportChallengeApplication>(*args)
}
