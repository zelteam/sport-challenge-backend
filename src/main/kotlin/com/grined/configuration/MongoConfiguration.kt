package com.grined.configuration

import com.mongodb.MongoClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import org.springframework.data.mongodb.gridfs.GridFsTemplate


@Configuration
class MongoConfiguration : AbstractMongoConfiguration() {
    @Value("\${mongo.db.name}")
    var dbName: String = ""

    @Value("\${mongo.db.server}")
    var databaseServer: String = ""

    @Bean
    @Throws(Exception::class)
    fun gridFsTemplate(): GridFsTemplate {
        return GridFsTemplate(mongoDbFactory(), mappingMongoConverter())
    }

    override fun mongoClient(): MongoClient {
        return MongoClient(databaseServer)
    }

    override fun getDatabaseName(): String {
        return dbName
    }
}




