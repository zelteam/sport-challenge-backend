package com.grined.configuration.security

import com.grined.configuration.security.filter.JWTAuthenticationFilter
import com.grined.configuration.security.filter.JWTLoginFilter
import com.grined.entity.Role
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class Security(val jwtAuthFilter: JWTAuthenticationFilter) : WebSecurityConfigurerAdapter() {

    @Autowired
    @Throws(Exception::class)
    fun configureGlobal(auth: AuthenticationManagerBuilder, userService: UserSecurityService) {
        auth
                .userDetailsService(userService)
                .passwordEncoder(passwordEncoder())
    }

    @Bean fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()


    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        // @formatter:off
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        http.csrf().disable()
             .authorizeRequests()
                .antMatchers("/open").permitAll()
                .antMatchers("/api/v1/user/create").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/login").permitAll()
                .antMatchers("/api/v1/user/all").hasAuthority(Role.ADMIN.name)
                .anyRequest().authenticated()
            .and()
            .addFilterBefore(JWTLoginFilter("/api/v1/login", authenticationManager()),
                    UsernamePasswordAuthenticationFilter::class.java)
            .addFilterBefore(jwtAuthFilter,
                    UsernamePasswordAuthenticationFilter::class.java);
        // @formatter:on
    }

}




