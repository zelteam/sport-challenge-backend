package com.grined.configuration.security

import com.grined.entity.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class SecUserDetails(val user : User) : UserDetails {
    override fun getAuthorities() = emptyList<GrantedAuthority>()
    override fun getPassword() = user.password
    override fun getUsername() = user.username

    override fun isEnabled() = true
    override fun isCredentialsNonExpired()= true
    override fun isAccountNonLocked()= true
    override fun isAccountNonExpired()= true
}