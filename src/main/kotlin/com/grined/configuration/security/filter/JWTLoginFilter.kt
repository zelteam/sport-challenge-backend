package com.grined.configuration.security.filter

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.grined.configuration.security.TokenService
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTLoginFilter(url: String, val authManager: AuthenticationManager) :
        AbstractAuthenticationProcessingFilter(AntPathRequestMatcher(url)) {


    @Throws(AuthenticationException::class, IOException::class, ServletException::class)
    override fun attemptAuthentication(req: HttpServletRequest, res: HttpServletResponse): Authentication {
        val creds : UserLoginDto = ObjectMapper().registerKotlinModule().readValue(req.inputStream)
        return authManager.authenticate(UsernamePasswordAuthenticationToken(creds.username, creds.password))
    }

    @Throws(IOException::class, ServletException::class)
    override fun successfulAuthentication(req: HttpServletRequest, res: HttpServletResponse,
                                          chain: FilterChain, auth: Authentication) {
        TokenService.addAuthentication(res, auth.name)
    }

    data class UserLoginDto(val username: String, val password: String)
}