package com.grined.configuration.security.filter

import com.grined.configuration.security.TokenService
import com.grined.repository.UserRepository
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

@Component
class JWTAuthenticationFilter(val userRepository: UserRepository) : GenericFilterBean() {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(request: ServletRequest, response: ServletResponse, filterChain: FilterChain) {
        val userName = TokenService.getAuthenticatedUser(request as HttpServletRequest)
        if (userName != null) {
            val user = userRepository.findByUsername(userName)!!
            SecurityContextHolder.getContext().authentication =
                    UsernamePasswordAuthenticationToken(userName, null,
                            listOf(SimpleGrantedAuthority(user.role.name)))
        }
        filterChain.doFilter(request, response)
    }
}