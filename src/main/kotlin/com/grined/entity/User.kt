package com.grined.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class User(
        @Id
        val id: String? = null,
        val username: String,
        val role:Role = Role.USER,
        val password: String,
        val firstName: String? = null,
        val lastName: String? = null
)

enum class Role {
        USER, ADMIN
}