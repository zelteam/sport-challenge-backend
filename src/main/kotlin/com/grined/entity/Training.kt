package com.grined.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class Training (
        @Id
        val id: String? = null,
        val userId: String,
        val likes: Int = 0,
        val disputes: Int = 0,
        val time: Instant,
        val metaInfo: TrainingMetaInfo? = null,
        val photoId: String? = null
)

data class TrainingMetaInfo(val durationSeconds : Int,
                            val description : String = "",
                            val longitude : Double,
                            val latitude : Double);