package com.grined.repository

import com.grined.entity.Training
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface TrainingRepository : PagingAndSortingRepository<Training, String> {
    fun findTrainingByUserId(userId : String) : List<Training>
}